package com.tgcs.demo.gw;

import com.tgcs.demo.gw.filter.PostCustomFilter;
import com.tgcs.demo.gw.filter.PreCustomFilter;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class GwDemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(GwDemoApplication.class, args);
	}

}
